### INPUT of cosmoRate #####
import sys
import numpy as np

###########################################################
# Description of input files
# The code will search files divided per each compact object class and per each Z
# in a folder called '../astromodel/'+sim.
# The files must contain in the first colum the delay time in YEARS.
# All other columns can contain other binary intrinsic parameters. More details follow.
# WARNING: if you are including the masses of the compact objects, please be sure
# that the first mass is always the most massive!
# The first row of each file containes the total simulated mass.
############################################################

# Compact objects specificities (PopSynth / N-body)
# list of compact objects type
cb_class = ['BBHs'] # it can be either 'BBHs', 'BHNS', 'BNSs' 

# list of metallicity used from simulations, as float type, ascending order
# uncomment what you need!
Z_simulated = [0.0002, 0.0004, 0.0008, 0.0012, 0.0016, 0.002, 0.004, 0.006, 0.008, 0.012, 0.016, 0.02]
#Z_simulated = [0.0001, 0.0002, 0.0004, 0.0006, 0.0008, 0.001, 0.002,  0.004, 0.006, 0.008, 0.01, 0.014,  0.017, 0.02, 0.03]
#Z_simulated = [1e-11]
#Z_simulated = [0.0001]


formation_channel = 'iso'  # formation channel type (iso, dyn, popIII)

# this parameter is used to select your astrophysical model (i.e values of alpha common envelope)
sim = 'popI_II/A3/'
# it can be either sys.argv[1] or a string ( e.g. 'A5',
# 'popI_II/popII_I_for_comp/fiducial_pisnfarmer19/A1.0',  
#'popIII/popIII_v3/WOOSLEY/LOGFLAT/5_550_1/Z0.00000000001A1/')


sim_name = '_default_' # it can be equal to sim or selected by the user 
              # WARNING: if sim is a path, sim_name must be not a path!
extension = '.txt' # Extension of the outputfile. Also include the dot

###########################################################
# Definition of redshift bins. 
# you can choose between equally spaced in time (proper_time = True) 
# or in redshift (proper_time = False)
# the former has higher definition at lower redshift, 
# the latter the contrary
proper_time = False
if proper_time == False:
    #bin_z = 0.1  # redshift bin width
    N_z_bins = 100 # 100 for popII-I,  400 for popIII
    zMAX = 15  # maximum redshift 15 for popII-I, 40 for popII
    zMIN = 0  # minimum redshift
else:
    t_max_arr = 13.75 #Gyr, this can be at most equal to the age of the Universe
    t_min_arr = 1e-3 #Gyr, this cannot be lower than 1e-3 Gyr
    N_t_bins = 150 # total numer of bins

    
##########################################################
# select the population of stars 

pop = 'I-II' # it can be 'III' or 'I-II'


# for popI-II star, 'MandF2017' is the only option available
# for popIII you can choose from many options:
# 1. 'A-sloth_smooth_off'
# 2. 'Hartwig16'
# 3. 'DeSouza_smooth_off'
# 4. 'Jaacks'
# 5. 'LiuBromm'
# for other options see cosmo_rate_notebook.ipynb
# or sys.argv[2]
SFRD_model = 'MandF2017' 


###########################################################
# Metallicity and Star formation rate specificities 
# WARNING: ignore them if you are using the case popIII

# Choose the model for average metallicity fit type 
metmod_type = 'MandF2017'   # it can be 'linear' or 'MandF2017'
sigma_met = 0.20  # value of the metallicity spread in log-sun-scale
                  # it can be 0.10, 0.20, 0.30 or sys.argv[3]

# Definition of Solar metallicity
Z_sun = 0.019  # 0.0153 from Cauffau et al. 2011  
                # 0.019 from Gallazzi et al. 2008

# Here you set if cosmo_Rate evaluates also the uncertainty through a Monte Carlo approach
N_iter = 1  # number of iteration to be done for evaluating the uncertainty. In any case, it must be at least one!
# 1000 iterations are enough to perform a robust estimation of the uncertainties

# metallicity
met_uncertainty = 'No'  # if 'Yes' the uncertainty due to metallicity will be computed
met_slope_sprd = 0.14   # 0.14 for linear metallicity model or 0.014 from 'MandF2017'
met_inter_sprd = 0.14   # from Gallazzi et al. 2008

# star formation rate option
sfr_uncertainty = 'No'  # if 'Yes' the uncertainty due to sfr will be computed,
# spread over the normalisation value 
sfr_sprd = 0.2  # from Madau & Dickinson 2017

###########################################################
# Catalogs of merging compact object options

catalogues_c = 'No'  # if 'Yes' merging compact object per redshift bin are produced
mrd_eval = 'Yes' # this option is to skip the evaluation of the merger rate density in case
                 # it has been already done in previous runs
    
# specify the redshift bins in which you want to have the catalogs printed
# WARNING: The code will search for the closest available redshift bin!
z_vect_cat_op =  False
# some other options:
#False
#[14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0.1] 
# [20, 11, 5, 0.1]
# if this option is set to False the catalogs will be printed at every merger redshift    
#[30, 25, 20, 17, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0.1] 
# if you use np.arange remember to add .tolist() at the end otherwise you'll get an error,
# es. np.arange(0,40,0.2).tolist() 
    
# Specify what parameters cosmoRate will sample. The order of the parameter names must be exactly 
# the same as the order of the corresponding column in input_catalogs/
# delay time is sampled by default. 
# The number of parameters can be smaller than the total number of paramters given as input.
# Leave parameters = [] if in your catalogs you want just time-delay related parameters,
# i.e. merging redshift, formation redshift, delay time and progenitor star metallicity
parameters = ['M1[Msun]', 'M2[Msun]']  
            

# You can choose to sample from a different distirbution percentile than the median value.
# If you run cosmoRate for the first time, switch the uncertainty options on.
perc_input = 50  # Specify here the percentile value
# if the uncertainty has already been evaluated the code will look automatically 
# for the uncertianty output file.
# Specify here the uncertianty type you have already evaluated.
met_unc_cat = 'No'
sfr_unc_cat = 'No'
# You might have parallelized the code to evaluate the uncertainty
n_paral_cat = int(1)  # total number of parallelized files,
# e.g. if they are file_0.dat and file_1.dat, n_paral_cat = 2
# number of iterations per each parallelization
N_iter_par = N_iter 
N_cat = 2e05 # number of CB per redshift bin
# number of significant decimal digits that are saved for each paramter of the merging compact binary catalogue. 
# This feature has been provided for saving memory space.
N_figures = 5


sampling = 'SFR_pw' # it can be 'SFR_pw', where pw stands for "properly weighted"
                    # or 'uniform'

# while printing the catalogs gives more info
verbose = False

###########################################################
# Merger efficiency interpolation

# if 'Yes' the code evaluates the MRD using the merging efficiency interpolated from Z_simulated on Z_interpol
eta_interpol_c = 'No'
# values of metallicity on which the code performs merging efficiency interpolation, as float, ascending order
Z_interpol = [0.0009, 0.009, 0.018]

# path where to write output file
# WARNING: it must include a terminal slash '/'!
path_out_out = '/output_cosmo_Rate/'  # path to the output folder. The output are divided by CB class


# If 'Yes', cosmoRate will display some plots 
# in order:
# 1) (if option is activated) you can check by eye if choosen iteration is enough close 
#     the real merger rate density evalauted at perc_input percentile value.
#  
# 2) merger rate density
#
# 3) (if option is activated) primary mass distirbution at different redshift 
#
# The figures are in any case saved as a pfd file automatically. You can visualise it later on.
# either 'Yes' or 'No'
print_opt = 'Yes'

####
# This parameter prevents outputs to be overwritten if you intend to parallelise the code
n_paral = '1'

