#################################################
# This script contains the whole set of functions
# that are necessary to comso_rate_notebook.ipynb
#################################################
import bisect
import random
import astropy.units as u
from astropy.cosmology import Planck18 as Planck
from math import erf
import numpy as np
import matplotlib.pyplot as plt
from astropy.cosmology import z_at_value
from tqdm import tqdm


def merger_rate_density(metz, bin_z, z_hist, X_prob, bin_t, td_Z, eta, met_slope, met_inter, sfr_norm, sigma_met,
                       disable_c_in, SFR_eval, sfr_args_chosen):
  
    # This is the most important function of cosmo_Rate. It evaluates the merger rate density (MRD)
    # ----------------------
    # metz --> function that evaluates the mean metallicity evolution with redshift. 
    # bin_z --> width of the redshift bin. 
    # z_hist --> array of redshift binning
    # X_prob --> This array containes the metallicity values on which the metalicity probability distirbution is evaluated
    # bin_t --> array of lookback time binning
    # td_Z --> list of compact binaries delay times, divided by metallicity.
    # eta --> merger efficiency. 
    # met_slope --> slope of the metallicity evolution model
    # met_inter --> intercept of the metallicity evolution model
    # sfr_norm --> normalisation of the star formation rate
    # sigma_met --> metallicity spread that is going to be used in the gaussian distribution of metallicity values
    #               at each given redshift
    # disable_c_in --> activate the progress bar
    # ----------------------


    
    
    len_Zlist = len(X_prob)-1  # This is equal to the metallicity array length.

    # Initialise quantities
    MRD = np.zeros((len(z_hist))-1)
    w = (len(z_hist)-1, len(z_hist)-1, len_Zlist)
    v = np.zeros(w)

    # numerical derivative over time at the merger redshift
    delta_t_merg = np.zeros(len(z_hist)-1)
    for d in range(len(delta_t_merg)): 
      delta_t_merg[d] = bin_t[len(z_hist)-d-1]-bin_t[len(z_hist)-d-1-1]
      #print(delta_t_merg[d])
      
    # loop over formation redshift
    for i in tqdm(range(len(bin_z)), disable = disable_c_in):
        
        
        # value of the formation redshift in the middle of the redshift bin
        zf_bin = z_hist[i]-bin_z[i]/2
        # compute associated formation time in yr
        t_form = np.array(Planck.lookback_time(zf_bin)/u.Gyr*10**9)
        
        SFR = SFR_eval(zf_bin, sfr_args_chosen)  # Msun yr-1 Mpc-3

        # compute the mass density of compact objects by multypling star formation rate with the associated time in the redshift bin        
        H0inv = 1./Planck.H0.cgs
        H0inv = H0inv.to(u.yr).value  # 1/H0 in units of yr
        dtdz = ((1.+zf_bin)* ((1.+zf_bin)**3.*Planck.Om0+(1.-Planck.Om0))**0.5)
        dtdz = H0inv/dtdz   # dtdz is 1/H0 1/(1+z) 1/((1+z)**3 *Om+ Ol)**0.5
        Mdens = SFR * dtdz * bin_z[i]  # Msun Mpc-3
        #print(SFR, dtdz)
        # compute value of average metallicity in the redshift bin. The metallicity is assumed to be log-normal distributed.
        # metz gives the result in log-sun scale
        mu = metz(zf_bin, met_slope, met_inter, sigma_met)

        # compute the probability for each metallicity using cumulative distribution functions
        prob = np.zeros(len(X_prob)-1)
        for p in range(len(X_prob)-1):
            xA = X_prob[p]
            probA = 0.5*(1+erf((xA - mu)/(sigma_met*2**0.5)))
            xB = X_prob[p+1]
            probB = 0.5*(1+erf((xB - mu)/(sigma_met*2**0.5)))
            prob[p] = probB-probA
            
            
        if sum(prob) < 0.98:
            raise Exception('sum of all probabilities = ',
                  sum(prob), 'at formation z = ', zf)
        # loop over each metallicity
        for m in range(len_Zlist):

            tdm = td_Z[m]  # Delay time of compact binaries
            t_merg = t_form - tdm  # time of merger yr

            # build histogram of the merger times of the compact objects mergers
            [vv, base] = np.histogram(t_merg, bins=bin_t)
            
            
            # The next quantity gives us the number of objects merging per Mpc-3 as a function of 3 variables.
            # The first index tracks the merger time bin, the second the time bin of formation time and the final one the metallicity
            # vv[::-1]/float(len(tdm)) gives the proportion of mergers formed at zf that merges in each zm. by multipyling it with the merger efficiency eta (Msol-1),
            # the probability that the metallicity was m at zf and the mass density at zf, we obtain the density of objects per Mpc-3 that formed at zf with met Z and merged at zm.
            v[:, i, m] = vv[::-1]/float(len(tdm))*eta[m]*prob[m]*Mdens/delta_t_merg

    # compute the merger rate density for each zm, by summing all formation redshift up to zm and all metallicities.
    for d in range(0, len(z_hist)-1, 1):  # k selects the merging redshift
        rate = 0
        for q in range(0, d + 1, 1):  # k+1 in order to reach diagonal values
            rate = rate + sum(v[d, q, :])
        MRD[d] = rate*1e9 # the 1e9 is to convert the merger rate density in Gpc^-3

    return MRD, v 


def eta_interpol(Z_missing, Z_simulated, eta_known):
    # This function provides the merger efficiency interpolation
    # ---------------
    # Z_missing --> list of metallicities on which the interpolation is to be performed
    # Z_simulated --> list of metallicity that has been used in the simulation
    # eta_known --> merger efficiency that is evaluated in cosmo_rate.py, with the data provided by the simulations
    # ---------------

    # interpolation of the merging efficiency on the provided values
    eta_unknown_log = np.interp(
        np.log10(Z_missing), np.log10(Z_simulated), np.log10(eta_known))
    eta_unknown = 10**(eta_unknown_log)
    Z_list_us = np.concatenate((Z_simulated, Z_missing))
    Z_list = np.sort(Z_list_us)
    Z_list_index = np.argsort(Z_list_us)
    eta_us = np.concatenate((eta_known, eta_unknown))
    eta = [eta_us[Z_list_index[i]] for i in range(len(Z_list_index))]
    return eta, Z_list


def sort_catalogues(Z_list, Z_simulated, td_Z_sim, data_Z_sim):
    # This function provides the delay time and data arrays once the eta_interpolation.py has been launched
    # --------------
    # Z_list --> list of all metallicities, both missing and simulated (see above function)
    # Z_simulated --> list of simulated metalicity only
    # td_Z_sim --> time delays of simulated compact binaries.
    # data_Z_sim --> all other consider parameters of simulated compact binaries
    # --------------

    td_Z = np.zeros((len(Z_list)), dtype=np.ndarray)
    data_Z = np.zeros((len(Z_list)), dtype=np.ndarray)
    index = np.zeros((len(Z_list)))

    Z_list = Z_list.tolist()

    for i in range(len(Z_list)):
        err = 100.
        if Z_list[i] <= 0.002:
            for met in Z_simulated:
                if err > np.abs(Z_list[i] - met):
                    err = np.abs(Z_list[i] - met)
                    index[i] = Z_simulated.index(met)
        else:
            index[i] = Z_simulated.index(0.02)

    index = [int(index[y]) for y in range(len(index))]
    td_Z = [td_Z_sim[index[y]] for y in range(len(Z_list))]
    data_Z = [data_Z_sim[index[y]] for y in range(len(Z_list))]

    return td_Z, data_Z


def catalogues(z_hist, z_vect_cat, bin_z, Z_list, v, N_cat, tdZ, dataZ, variables, location, objn, sim, SFRD_name, N_figures, perc_input, SFR, sfr_args_chosen, sampling, verbose, bin_t): 
    # This function creates the catalogues of merging compact objects as predicted by this model
    # It gives as output a file with N_cat (cfr cosmo_input.py)
    # compact binaries, a Z_perc catalogue where the percentage distribution are displayed

    # -------------
    # z_hist --> array of redshift binning
    # z_vect_cat --> array of redshift binning on which to print the catalogs
    # bin_z --> array of redshift width
    # Z_list --> list of metallicities
    # v --> matrix with three dimensions: formation redshift, merging redshift, metallicity. It is an output of merger_rate_density. It contains the merger rate density before summation
    # N_cat --> number of binaries per each catalogues
    # tdZ --> compact binaries delay times, divided by metallicity
    # dataZ --> all other compact binaries paramters, divided by metallicity
    # location --> folder location in which the catalogues must be save
    # objn --> compact binary class (BBH, BHNS and BNS)
    # sim --> name of the simulation as given in cosmo_input.py
    # N_figures --> number of significant decimal digits that are saved
    # perc_input --> percentile from which the catalgoues are sampled
    # ---------------

    # initialise
    
    
    # in this version I am also taking in account the formation redshift
    Znum = np.zeros((len(z_vect_cat), len(z_hist)-1, len(Z_list)))
    Zperc = np.zeros((len(z_vect_cat), len(z_hist)-1, len(Z_list)))
    Ztot = np.zeros(len(z_vect_cat))
    

    # maximum formation redshift (kinda the age of the Universe in this case)
    #t_Universe = Planck.lookback_time(max(z_hist))/u.Gyr*1e9 
    
    # loop on merger redshift
    for k in tqdm(range(len(z_vect_cat)), disable=False):
      
        z_merge_select = z_vect_cat[k]      
      
        # this index defines the merger redshift
        ind_met = np.argmin(abs(z_merge_select-(z_hist[0:-1]-bin_z/2)))        
        z_merge = z_hist[ind_met] - bin_z[ind_met]/2
        if verbose == True:
          print('z catalog = ',z_vect_cat[k], 'at z_merg to be saved = ', z_merge)

        if np.sum(v[ind_met, :, :], axis = None) < 1e-15:
            print('When merger rate density == 0, catalogues are not produced')
            continue
        #else:
        #    print(np.sum(v[ind_met, :, :]))
          
        if sampling == 'SFR_pw':

          # The quantity Znum corresponds to [...]
          for c in range(len(Z_list)):
              # now it goes also on the formation redshift 
              # the max index must be ind_met+1 otherwise you count mergers that are not there...
              for cc in range(0, ind_met+1, 1):
                Znum[k, cc, c] = v[ind_met, cc, c]
                if Znum[k,cc,c] < 0:
                  print('some issue with distribution of metallicities 1/2')
                  raise

          # k goes over z_vet_cat and it is right!
          Ztot[k] = np.sum(Znum[k, :, :], axis = None)
          if Ztot[k] == 0:
            print('When merger rate density == 0, catalogues are not produced')
            continue
            #print(Ztot[k])
            #print('some issue with distribution of metallicities 2/2')
            #raise 
          #print(Ztot[k])
          Zperc[k, :, :] = Znum[k, :, :]*100/Ztot[k]

          #print(Zperc.shape)

          if verbose == True:
            print('sum of Z_perc = ', np.sum(Zperc[k,:,:], axis = None))



          # contain the values of the parameter.
          cat = np.zeros((int(N_cat), 4+len(variables)))

          """
          # This was the old method implemented with Yann,
          # now it is more refined, as it taks in account also the formation rate
          source_number = np.zeros((len(z_hist)-1,len(Z_list)), dtype = int)
          for r in range(int(N_cat)):
              #Zrand = np.random.uniform(0, 100)
              #Zsampling = 0
              for d in range((len(Z_list))):
                  Zrand = np.random.uniform(0, 100)
                  Zsampling = 0
                  for dd in range((len(z_hist)-1)):
                    Zsampling = Zsampling + Zperc[k,dd,d]
                    if Zrand < Zsampling:
                        source_number[dd,d] += 1
                        break
          """

          aa = np.arange(0,(len(z_hist)-1)*len(Z_list),1, dtype = int)
          #print('aa = ', len(aa))
          pp = Zperc[k,:,:].flatten() #.reshape((len(z_hist)-1)*len(Z_list)) 
          #print('pp = ', len(pp))
          source_number_sample = np.random.choice(aa,
                                                  replace = True, 
                                                  size = int(N_cat),
                                                  p = pp/100) # weights must sum to 1

          #print('tot sources = ', len(source_number_sample))


          source_number = np.zeros((len(z_hist)-1,len(Z_list)), dtype = int)
          count_s = 0
          for dd in range(0,ind_met+1,1):
            for d in range((len(Z_list))):
          #    if source_number_sample[count] == count:
              source_number[dd,d] = len(np.nonzero(source_number_sample == count_s)[0])
              #print('number of sources = ', source_number[dd,d], 'at z_hist ', z_hist[dd], 'at Z = ', Z_list[d])
              count_s = count_s + 1


          tot_source =  np.sum(source_number[:,:], axis = None)
          if tot_source != N_cat:           
            print('total number of sources = ', np.sum(source_number[:,:], axis = None),
                 'while N_cat = ', N_cat)
            raise

          # this counts the total number of sampled parameters
          count = 0
          wrong = 0

          # at fixed metallicity
          sum_Z_perc = 0
          sum_ind_t_2 = 0
          for d in range(len(Z_list)):
              data_rem = dataZ[d]
              td = tdZ[d]          

              # This loop goes on the formation redshift
              for dd in range(0,ind_met+1,1):

                z_form = z_hist[dd]-bin_z[dd]/2   
                t_form = Planck.lookback_time(z_form)/u.Gyr*1e9 # in yr

                t_merg = t_form - td

                # This is a positive number 
                #delta_t_merg[d] = bin_t[len(z_hist)-d-1]-bin_t[len(z_hist)-d-1-1]

                ind_t_2 = np.nonzero((t_merg < bin_t[len(z_hist)-ind_met-1]) & (t_merg > bin_t[len(z_hist)-ind_met-1-1]))[0]


                sum_Z_perc = sum_Z_perc + Zperc[k,dd,d]
                sum_ind_t_2 = sum_ind_t_2+len(ind_t_2)

                if verbose == True:
                  print('# avail. sour. = ', len(ind_t_2), 'at z_form = ', z_form, '# sour. = ', source_number[dd,d], 'Z% = ', sum_Z_perc, 'at Z = ', Z_list[d])



                if len(ind_t_2) > 0:

                  for g in tqdm(range(int(source_number[dd,d])), disable = True):

                    ind_rand_index = np.random.choice(ind_t_2)     
                    # Here I am sampling the binary parameters
                    data_sel = data_rem[ind_rand_index, :]
                    td_sam = td[ind_rand_index]                
                    # reshuffle z_merge within the same redshift bin? it is not necessary for the moment
                    z_merg_save = z_merge #np.random.uniform(z_hist[k]-bin_z[k],z_hist[k])

                    # checks on formation and merger redshift
                    if (z_merg_save > z_form) or (z_merg_save > z_hist[ind_met]) or (z_merg_save < z_hist[ind_met]-bin_z[ind_met]):
                        if (abs(z_merg_save-z_hist[ind_met]) > 1e-04) and (abs(z_merg_save-(z_hist[ind_met]-bin_z[ind_met])) > 1e-04):
                          print('There is a problem with redhsifts! z_form = ', z_form,
                                  'while z_merg = ', z_merg_save, 'which should be within = ', z_hist[ind_met], 'and ', z_hist[ind_met]-bin_z[ind_met])
                          raise

                    cat[count, :] = np.concatenate((data_sel, z_merg_save, z_form, td_sam, Z_list[d]), axis = None)
                    count = count + 1
                    
        
        ##########
        ##
        #########
        # here we implement uniform distribution as it was before this update            
        
        elif sampling == 'uniform':
                    
          # The quantity Znum corresponds to [...]
          for c in range(len(Z_list)):
              # now it goes also on the formation redshift 
              # the max index must be ind_met+1 otherwise you count mergers that are not there...
              for cc in range(0, ind_met+1, 1):
                Znum[k, cc, c] = sum(v[ind_met, :, c])
                if Znum[k,cc,c] < 0:
                  print('some issue with distribution of metallicities')
                  raise

          # k goes over z_vet_cat and it is right!
          Ztot[k] = np.sum(Znum[k, :, :], axis = None)
          if Ztot[k] == 0:
            print('When merger rate density == 0, catalogues are not produced')
            continue
          
          #print(Ztot[k])
          
          
          Zperc[k, :, :] = Znum[k, :, :]*100/Ztot[k]

          #print(Zperc.shape)

          if verbose == True:
            print('sum of Z_perc = ', np.sum(Zperc[k,:,:], axis = None))



          # contain the values of the parameter.
          cat = np.zeros((int(N_cat), 4+len(variables)))
          
          aa = np.arange(0,(len(z_hist)-1)*len(Z_list),1, dtype = int)
          #print('aa = ', len(aa))
          pp = Zperc[k,:,:].flatten() #.reshape((len(z_hist)-1)*len(Z_list)) 
          #print('pp = ', len(pp))
          source_number_sample = np.random.choice(aa,
                                                  replace = True, 
                                                  size = int(N_cat),
                                                  p = pp/100) # weights must sum to 1

          #print('tot sources = ', len(source_number_sample))


          source_number = np.zeros((len(z_hist)-1,len(Z_list)), dtype = int)
          count_s = 0
          for dd in range(0,ind_met+1,1):
            for d in range((len(Z_list))):
          #    if source_number_sample[count] == count:
              source_number[dd,d] = len(np.nonzero(source_number_sample == count_s)[0])
              #print('number of sources = ', source_number[dd,d], 'at z_hist ', z_hist[dd], 'at Z = ', Z_list[d])
              count_s = count_s + 1


          tot_source =  np.sum(source_number[:,:], axis = None)
          if tot_source != N_cat:           
            print('total number of sources = ', np.sum(source_number[:,:], axis = None),
                 'while N_cat = ', N_cat)
            raise

          # this counts the total number of sampled parameters
          count = 0
          wrong = 0

          # at fixed metallicity
          sum_Z_perc = 0
          sum_ind_t_2 = 0
          for d in range(len(Z_list)):
              data_rem = dataZ[d]
              td = tdZ[d]          

              # This loop goes on the formation redshift
              for dd in range(0,ind_met+1,1):

                z_Universe = z_hist[0]-bin_z[0]/2   
                #z_form = z_hist-bin_z/2   
                t_Universe = Planck.lookback_time(z_Universe)/u.Gyr*1e9 # in yr
                t_merg =  t_Universe - td
                ind_t_2 = np.nonzero(t_merg > bin_t[len(z_hist)-ind_met-1-1])[0]
                #  (t_merg < bin_t[len(z_hist)-ind_met-1]) & (t_merg > bin_t[len(z_hist)-ind_met-1-1]))[0]

                """
                # in old version:
                # here I am selecting all the compact object that could merge in this redshift bin
                #ind_t_2 = np.nonzero(
                #  # this option selects only binaries that can merge within t_merg
                #  (td < (t_Universe - t_merg))              
                #  &
                #  # this option instead selects only binaries that can form within t_min
                #  (td > (t_min - t_merg)))[0]
                """


                sum_Z_perc = sum_Z_perc + Zperc[k,dd,d]
                sum_ind_t_2 = sum_ind_t_2+len(ind_t_2)

                if verbose == True:
                  print('# avail. sour. = ', len(ind_t_2), 'at z_form = ', z_Universe, '# sour. = ', source_number[dd,d], 'Z% = ', sum_Z_perc, 'at Z = ', Z_list[d])



                if len(ind_t_2) > 0:

                  for g in tqdm(range(int(source_number[dd,d])), disable = True):

                    ind_rand_index = np.random.choice(ind_t_2)     
                    # Here I am sampling the binary parameters
                    data_sel = data_rem[ind_rand_index, :]
                    td_sam = td[ind_rand_index]                
                    # reshuffle z_merge within the same redshift bin? it is not necessary for the moment
                    z_merg_save = z_merge #np.random.uniform(z_hist[k]-bin_z[k],z_hist[k])

                    #t_form = (t_merg + td_sam)/1e9 # in Gyr                
                    #z_form_t = z_at_value(Planck.lookback_time, t_form*u.Gyr)
                    # now I want to select the closest redshif bins
                    #ind_z_form = np.argmin(abs(z_form_t-(z_hist[1:]-bin_z/2)))
                    #z_form = z_hist[ind_z_form]-bin_z[ind_z_form]/2

                    z_form = z_Universe

                    # checks on formation and merger redshift
                    if (z_merg_save > z_form) or (z_merg_save > z_hist[ind_met]) or (z_merg_save < z_hist[ind_met]-bin_z[ind_met]):
                        if (abs(z_merg_save-z_hist[ind_met]) > 1e-04) and (abs(z_merg_save-(z_hist[ind_met]-bin_z[ind_met])) > 1e-04):
                          print('There is a problem with redhsifts! z_form = ', z_form,
                                  'while z_merg = ', z_merg_save, 'which should be within = ', z_hist[ind_met], 'and ', z_hist[ind_met]-bin_z[ind_met])
                          raise

                    cat[count, :] = np.concatenate((data_sel, z_merg_save, z_form, td_sam, Z_list[d]), axis = None)
                    count = count + 1          
          
          
          
          
          
          
          
        # saving the catalogs
          
          
                     
        if count < N_cat:
          print('ERROR: total number of binaries in the catalog = ', count, 'instead of ', N_cat)
          raise 
      
        # here I am saving the catalogs at each merger redshift 
        head_cat = ''
        for i in range(len(variables)):
            head_cat = head_cat + variables[i] + ' '

        head_cat = head_cat+'z_merg'+' '+'z_form' + \
            ' '+'time_delay[yr]'+' '+'Z_progenitor'
        # numbers are rounded up to #N_figures significant decimal figures. See cosmo_input.py for further details
        np.savetxt(location+'/'+objn+'_'+sim+'_'+SFRD_name+'_' +\
                   #str(len(z_vect_cat)-k-1)
                   'z'+str(round(z_vect_cat[k],2))+'_'+str(perc_input)+'_'+sampling+'.dat', cat, header=head_cat, fmt='%1.'+str(N_figures)+'f', comments = '#')
        if wrong > 0:
            print(
                wrong, ' compact binaries were selected despite their time delay at redshift = ', z_vect_cat[k])
    return Zperc

#############
