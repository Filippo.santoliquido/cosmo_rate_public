**cosmoRate by Filippo Santoliquido**

Welcome to the user guide for cosmoRate! This open-source code is designed to evaluate the property distributions of compact object mergers across the history of the Universe. By following this guide, you'll learn how to use cosmoRate. 


**RELATED PAPERS:**

If you have any questions regarding the use of cosmoRate or need further assistance, please don't hesitate to reach out to the following contacts:
Filippo Santoliquido: filippo.santoliquido@unipd.it


Additionally, if you use cosmoRate for your publication, we kindly request that you include the following citations:
Santoliquido et al., 2020, ApJ - https://ui.adsabs.harvard.edu/abs/2020ApJ...898..152S/abstract
Santoliquido et al., 2021, MNRAS - https://ui.adsabs.harvard.edu/abs/2021MNRAS.502.4877S/abstract
These papers provide in-depth details about the code and the scientific output generated using cosmoRate. We encourage you to explore them for a comprehensive understanding of the code capabilities and insights.


**Installation**

To be sure cosmoRate works properly, you can create a conda environment including all the necessary libraries:
```
conda create --name cosmo_env python=3.9.19
conda activate cosmo_env
```
The following packages need to be installed:
```
pip install astropy==5.3.4
pip install matplotlib==3.8.4
pip install tqdm==4.66.4
pip install pandas==2.2.2
pip install scipy==1.13.1
pip install numpy==1.25
pip install ipython==8.15.0
pip install jupyter_client==8.6.0
pip install jupyter_core==5.7.2
```
The following command instals the conda environment on Jupyter lab:
```
python -m ipykernel install --user --name=cosmo_env --display-name "cosmo_env"
```

**INPUT**

To effectively organize your input in cosmoRate, please follow these guidelines:

1. Create a directory named "astromodel" to store all your astrophysical models. This will serve as the main directory for cosmoRate.

2. Within the "astromodel" directory, create subdirectories for different models. Each subdirectory should correspond to a specific astrophysical model and should have a descriptive name.

3. Eeach model subdirectory will contain the catalogs of compact object mergers, which should be further organized based on metallicity. For example, you can have files like "data_BBH_0.02.dat" for binary black holes with a metallicity of 0.02.

4. Additionally, within each model subdirectory, create a folder named "output_cosmo_Rate". This folder will contain the output of cosmoRate for each compact objcet class. Create a separate folder for each class, including binary black holes ('BBHs'), black hole-neutron stars ('BHNS'), and binary neutron stars ('BNSs'). The output files and any relevant data for each class should be stored within their respective folders.

An example is provided. The astrophysical model is called "A3". 

**RUN and FILES**

In the "cosmo_Rate_0/" directory, you will find a Jupyter notebook named "cosmo_rate_notebook.ipynb." This notebook contains the main code for evaluating the merger rate density as a function of redshift. cosmoRate provides you with the flexibility to explore the impact of various parameters on the merger rate density. Some of the parameters you can investigate include metallicity evolution, star-formation rate density, and many others. You can modify these parameters within the script "cosmo_params.py". Feel free to open "cosmo_params.py" to start exploring the different parameters and their influence on the merger rate density calculations.

WARNING! 

- if you change the cosmological parameters (e.g. Planck18) be sure to do it also in the file cosmo_functions.py.

- in cosmo_params.py, you can select different options for ```formation_channel```. Note that if you select ```'iso'``` you are impliciting setting ```f_imf = 0.285```. This is a parameter that accounts for the incomplete sampling of the initial mass function. If the total simulated mass that you insert in the first row of each input file (e.g. ```data_*_*.dat```) is already corrected, you need to set ```f_imf = 1``` in ```cosmo_rate_notebook.ipynb```)

**OUTPUT**

The main output file of cosmoRate is named "MRD_*options*.dat". This file contains the following columns:

Column 0: Redshift
Column 1: Merger rate density in units of [Gpc^-3, yr^-1]

If the user has activated the option to evaluate the uncertainties (i.e. ```met_uncertainty = 'Yes'``` and/or ```sfr_uncertainty = 'Yes'``` ), the above-mentioned file will have one extra column for each iteration for a total of ```N_iter``` iterations. 

If you have activated the corresponding option in cosmoRate, it will generate catalogs of merging compact objects at each given redshift. These catalogues are organized into six (or more) columns:

Column 1: Mass of the first compact object (which may not necessarily be the primary mass) in solar masses
Column 2: Mass of the second compact object in solar masses
Column 3: Merging redshift
Column 4: Formation redshift
Column 5: Delay time in years
Column 6: Progenitor metallicity
Starting from column 7, you will find other additional parameters related to the compact objects (e.g. spins, formation channels, etc.).


**Further details**

For more detailed instructions and additional information, you can refer to the wiki of cosmoRate and to [https://filippo-santoliquido.github.io/](https://filippo-santoliquido.github.io/).
You can find the user guide [here](https://filippo-santoliquido.github.io/assets/images/BrazilLectures/cosmorate_user_guide.pdf). Additionally, exercises are available:: [Exercise 1](https://filippo-santoliquido.github.io/Brazil/), [Exercise 2](https://filippo-santoliquido.github.io/chile/) 

**Developer Policy**
If you plan to initiate the development of ```cosmoRate```, we strongly encourage you to create a new fork. Doing so will facilitate the seamless integration of future code versions, ensuring users can stay up to date.
